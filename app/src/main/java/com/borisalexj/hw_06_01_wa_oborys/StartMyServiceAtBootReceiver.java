package com.borisalexj.hw_06_01_wa_oborys;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by user on 4/20/2017.
 */
public class StartMyServiceAtBootReceiver extends BroadcastReceiver {
    private String TAG = this.getClass().getSimpleName();

    private boolean checkLastScheduledSyncDate(Context context) {
        Log.d(TAG, "checkLastScheduledSyncDate: ");
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF_NAME, MODE_PRIVATE);
        String lastStoredDate = sharedPreferences.getString(Constants.SHARED_PREF_DATE, "");
        String lastStoredTime = sharedPreferences.getString(Constants.SHARED_PREF_TIME, "");
        Log.d(TAG, "checkLastScheduledSyncDate: " + lastStoredDate + " " + lastStoredTime);

        if (lastStoredDate != null && !lastStoredDate.equals("") && lastStoredDate.equals(Common.getCurrentPhoneDate())) {
            return true;
        }
        return false;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: ");
        if (Common.isWiFiPresent(context) && !checkLastScheduledSyncDate(context)) {
            Intent serviceIntent = new Intent(context, MyService.class);
            context.startService(serviceIntent);
        }
    }
}
