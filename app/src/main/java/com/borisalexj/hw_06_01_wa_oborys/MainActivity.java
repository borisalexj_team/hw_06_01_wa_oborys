package com.borisalexj.hw_06_01_wa_oborys;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ItemListAdapter mItemAdapter;
    private String TAG = this.getClass().getSimpleName();
    private ListView mListView;
    private ShowItems mItemShower;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = (ListView) findViewById(R.id.listView);

        mItemAdapter = new ItemListAdapter();
        mListView.setAdapter(mItemAdapter);

        mItemShower = new ShowItems();
    }

    private boolean checkLastScheduledSyncDate() {
        Log.d(TAG, "checkLastScheduledSyncDate: ");
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREF_NAME, MODE_PRIVATE);
        String lastStoredDate = sharedPreferences.getString(Constants.SHARED_PREF_DATE, "");
        String lastStoredTime = sharedPreferences.getString(Constants.SHARED_PREF_TIME, "");
        Log.d(TAG, "checkLastScheduledSyncDate: " + lastStoredDate + " " + lastStoredTime);

        ((TextView) findViewById(R.id.lastSyncDateTime)).setText(lastStoredDate + " " + lastStoredTime);

        if (lastStoredDate != null && !lastStoredDate.equals("") && lastStoredDate.equals(Common.getCurrentPhoneDate())) {
            return true;
        }
        return false;
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: ");
        super.onResume();

        if (checkLastScheduledSyncDate()) {
            loadAndShowStoredItems();
        } else {
            (new ApiMethods(getApplicationContext(), TAG)).makeRequestForApi(mItemShower);
        }
    }

    public void clearDbClick(View view) {
        (new DbMethods(this, TAG)).clearItems();
        mItemAdapter.setItemsArray(new ArrayList<ItemModel>());
        mItemAdapter.notifyDataSetChanged();
        checkLastScheduledSyncDate();
    }

    public void refreshClick(View view) {
        loadAndShowStoredItems();
    }

    private void loadAndShowStoredItems() {
        Log.d(TAG, "loadAndShowStoredItems: ");

        ArrayList<ItemModel> itemsArray = (new DbMethods(this, TAG)).getItems();
        mItemShower.show(itemsArray);
    }

    public void syncNowClick(View view) {
        (new ApiMethods(getApplicationContext(), TAG)).makeRequestForApi(mItemShower);
    }

    public void scheduleSyncClick(View view) {
        Common.scheduleSync(this, TAG);
    }


    public interface iShowItems {
        public void show(ArrayList<ItemModel> itemsArray);
    }

    private class ShowItems implements iShowItems {

        @Override
        public void show(ArrayList<ItemModel> itemsArray) {
            Log.d(TAG, "showItems: ");
            mItemAdapter.setItemsArray(itemsArray);
            mItemAdapter.notifyDataSetChanged();
            checkLastScheduledSyncDate();
        }
    }

    public class ItemListAdapter extends BaseAdapter {

        ArrayList<ItemModel> mItemArray;

        public ItemListAdapter() {
            mItemArray = new ArrayList<>();
        }

        public ItemListAdapter(ArrayList<ItemModel> itemArray) {
            this.mItemArray = itemArray;
        }

        public void setItemsArray(ArrayList<ItemModel> itemArray) {
            mItemArray = itemArray;
        }

        @Override
        public int getCount() {
            return mItemArray.size();
        }

        @Override
        public ItemModel getItem(int position) {
            return mItemArray.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) MainActivity.this.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_list_item, parent, false);

            ((TextView) view.findViewById(R.id.itemTitle)).setText(getItem(position).getTitle());
            ((TextView) view.findViewById(R.id.itemDateTaken)).setText(getItem(position).getDate_taken());
            ((TextView) view.findViewById(R.id.itemAuthor)).setText(getItem(position).getAuthor());
            ((TextView) view.findViewById(R.id.itemTags)).setText(getItem(position).getTags());

            Picasso.with(MainActivity.this.getApplicationContext()).load(getItem(position).getMedia())
                    .into(((ImageView) view.findViewById(R.id.itemMedia)));

            return view;
        }
    }
}
