package com.borisalexj.hw_06_01_wa_oborys;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by user on 4/19/2017.
 */

public interface iApi {
    @GET("{url}")
    Call<Object> request(@Path(value = "url", encoded = true) String url,
                         @Query("format") String format,
                         @Query("nojsoncallback") String nojsoncallback);
}
