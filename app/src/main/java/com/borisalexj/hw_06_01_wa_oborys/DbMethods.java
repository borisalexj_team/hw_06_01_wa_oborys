package com.borisalexj.hw_06_01_wa_oborys;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by user on 4/19/2017.
 */

public class DbMethods {
    private String TAG = this.getClass().getSimpleName();
    private Context mContext;

    public DbMethods(Context context, String tag) {
        this.mContext = context;
        TAG = tag + " " + this.getClass().getSimpleName();
    }

    public void clearItems() {
        Log.d(TAG, "clearItems: ");
        ItemDatabase itemDb = new ItemDatabase(mContext);
        SQLiteDatabase sqLiteItemDb = itemDb.getWritableDatabase();
        sqLiteItemDb.delete(ItemDatabase.DATABASE.ITEM_TABLE_NAME,
                null,
                null);
        itemDb.close();

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.SHARED_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(Constants.SHARED_PREF_DATE);
        editor.remove(Constants.SHARED_PREF_TIME);
        editor.apply();
    }

    public void storeItems(ArrayList<ItemModel> itemsArray) {
        Log.d(TAG, "storeItems: ");
        ItemDatabase itemDb = new ItemDatabase(mContext);
        SQLiteDatabase sqLiteItemDb = itemDb.getWritableDatabase();
        try {
            for (int i = 0; i < itemsArray.size(); i++) {
                sqLiteItemDb.insert(ItemDatabase.DATABASE.ITEM_TABLE_NAME, null, itemsArray.get(i).toDb());
            }
        } catch (Exception e) {
            Log.d(TAG, "storeItems: ");
            e.printStackTrace();
        } finally {
            sqLiteItemDb.close();
            itemDb.close();
        }
    }

    public ArrayList<ItemModel> getItems() {
        Log.d(TAG, "getItems: ");
        ArrayList<ItemModel> itemsArray = new ArrayList<>();
        ItemDatabase itemDb = new ItemDatabase(mContext);
        SQLiteDatabase sqLiteItemDb = itemDb.getReadableDatabase();

        Cursor cursor = sqLiteItemDb.query(ItemDatabase.DATABASE.ITEM_TABLE_NAME,
                null, null, null, null, null, null);
        cursor.moveToFirst();
        itemsArray.clear();
        if (cursor.getCount() != 0) {
            do {
                itemsArray.add(new ItemModel(
                        cursor.getString(cursor.getColumnIndex("title")),
                        cursor.getString(cursor.getColumnIndex("link")),
                        cursor.getString(cursor.getColumnIndex("media")),
                        cursor.getString(cursor.getColumnIndex("date_taken")),
                        cursor.getString(cursor.getColumnIndex("description")),
                        cursor.getString(cursor.getColumnIndex("published")),
                        cursor.getString(cursor.getColumnIndex("author")),
                        cursor.getString(cursor.getColumnIndex("author_id")),
                        cursor.getString(cursor.getColumnIndex("tags"))
                ));
            } while (cursor.moveToNext());
            Log.d(TAG, "getPointsFromDb: array_size - " + String.valueOf(itemsArray.size()));
        }
        itemDb.close();

        return itemsArray;
    }
}
