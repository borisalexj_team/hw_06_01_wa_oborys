package com.borisalexj.hw_06_01_wa_oborys;

import android.content.ContentValues;

/**
 * Created by user on 4/19/2017.
 */
public class ItemModel extends Object{
    private String title = "";
    private String link = "";
    private String media = "";
    private String date_taken = "";
    private String description = "";
    private String published = "";
    private String author = "";
    private String author_id = "";
    private String tags = "";

    public ItemModel() {
    }

    public ItemModel(String title, String link, String media, String date_taken, String description, String published, String author, String author_id, String tags) {
        this.title = title;
        this.link = link;
        this.media = media;
        this.date_taken = date_taken;
        this.description = description;
        this.published = published;
        this.author = author;
        this.author_id = author_id;
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "ItemModel{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", media='" + media + '\'' +
                ", date_taken='" + date_taken + '\'' +
                ", description='" + description + '\'' +
                ", published='" + published + '\'' +
                ", author='" + author + '\'' +
                ", author_id='" + author_id + '\'' +
                ", tags='" + tags + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getDate_taken() {
        return date_taken;
    }

    public void setDate_taken(String date_taken) {
        this.date_taken = date_taken;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(String author_id) {
        this.author_id = author_id;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public ContentValues toDb() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", title);
        contentValues.put("link", link);
        contentValues.put("media", media);
        contentValues.put("date_taken", date_taken);
        contentValues.put("description", description);
        contentValues.put("published", published);
        contentValues.put("author", author);
        contentValues.put("author_id", author_id);
        contentValues.put("tags",tags);
        return contentValues;
    }
}


//    title
//    link
//    media
//    date_taken
//    description
//    published
//    author
//    author_id
//    tags