package com.borisalexj.hw_06_01_wa_oborys;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by user on 4/19/2017.
 */

public class ItemDatabase extends SQLiteOpenHelper {
    private String TAG = this.getClass().getSimpleName();

    public ItemDatabase(Context context) {
        super(context, DATABASE.DB_NAME, null, DATABASE.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate: ");
        db.execSQL(DATABASE.CREATE_ITEM_TABLE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public static final class DATABASE {
        public static final int DB_VERSION = 1;
        public static final String DB_NAME = "item_db" + DB_VERSION;

        public static final String ITEM_TABLE_NAME = "items" + DB_VERSION;

        public static final String CREATE_ITEM_TABLE_QUERY = "create table " + ITEM_TABLE_NAME + "" +
                " (id integer primary key autoincrement," +
                " title text," +
                " link text," +
                " media text," +
                " date_taken text," +
                " description text," +
                " published text," +
                " author text," +
                " author_id text," +
                " tags text)";

    }
}
