package com.borisalexj.hw_06_01_wa_oborys;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by user on 4/20/2017.
 */

public class ApiMethods {
    private String TAG = "";
    private Context mContext;

    public ApiMethods(Context context, String tag) {
        mContext = context;
        TAG = tag;
    }

    public void makeRequestForApi(@Nullable final MainActivity.iShowItems callback) {
        Log.d(TAG, "makeRequestForApi: ");


        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(Constants.API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        );

        Retrofit retrofit = builder.client(httpClient.build()).build();
        iApi client = retrofit.create(iApi.class);

        Call<Object> call = client.request(Constants.API_METHOD_URL, "json", "1");

        call.enqueue(new Callback<Object>() {

            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.d(TAG, "onResponse: success");
                Log.d(TAG, "onResponse: " + call.request().url());
                Log.d(TAG, "onResponse: " + response);
                Log.d(TAG, "onResponse: " + response.body());

                JSONObject result;
                Gson gson = new GsonBuilder().setLenient().create();
                ArrayList<ItemModel> itemsArray = new ArrayList<ItemModel>();
                try {
                    result = new JSONObject(gson.toJson(response.body()));
                    Log.d(TAG, "onResponse: " + result);

                    JSONArray resultArray = result.getJSONArray("items");

                    itemsArray = parseResult(resultArray);

                    DbMethods dbMethods = new DbMethods(mContext, TAG);
                    dbMethods.clearItems();
                    dbMethods.storeItems(itemsArray);

                    if (callback != null) {
                        callback.show(itemsArray);
                    } else {
                        saveUpdateDate();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.d(TAG, "onResponse: onFailure");
                Log.d(TAG, "onResponse: " + call.request().url());
                Log.d(TAG, "onResponse: " + t);
            }
        });


    }

    private void saveUpdateDate() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.SHARED_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.SHARED_PREF_DATE, Common.getCurrentPhoneDate());
        editor.putString(Constants.SHARED_PREF_NAME, Common.getCurrentPhoneTime());
        editor.apply();
    }

    private ArrayList<ItemModel> parseResult(JSONArray resultArray) throws JSONException {
        ArrayList<ItemModel> itemsArray = new ArrayList<ItemModel>();
        for (int i = 0; i < resultArray.length(); i++) {
            JSONObject o = (JSONObject) resultArray.get(i);
            ItemModel im = new ItemModel(
                    o.getString("title").trim(),
                    o.getString("link").trim(),
                    o.getJSONObject("media").getString("m").trim(),
                    o.getString("date_taken").trim(),
                    o.getString("description").trim(),
                    o.getString("published").trim(),
                    o.getString("author").trim(),
                    o.getString("author_id").trim(),
                    o.getString("tags").trim());
            itemsArray.add(im);
            Log.d(TAG, "onResponse: " + im);
        }

        return itemsArray;
    }
}
