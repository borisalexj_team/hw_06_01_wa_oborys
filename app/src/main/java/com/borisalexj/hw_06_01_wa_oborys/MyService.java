package com.borisalexj.hw_06_01_wa_oborys;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by user on 4/20/2017.
 */
public class MyService extends Service {
    private String TAG = this.getClass().getSimpleName();
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind: ");
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate: ");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: ");
        Common.scheduleSync(this, TAG);
        (new ApiMethods(getApplicationContext(), TAG)).makeRequestForApi(null);
        return super.onStartCommand(intent, flags, startId);
    }
}
