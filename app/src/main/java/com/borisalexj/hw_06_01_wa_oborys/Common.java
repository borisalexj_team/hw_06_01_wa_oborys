package com.borisalexj.hw_06_01_wa_oborys;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by user on 4/20/2017.
 */

public class Common {
    public static String getCurrentPhoneDate() {
        Calendar c = Calendar.getInstance();
        Date currentDateTime = c.getTime();
        SimpleDateFormat outFormat = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.ENGLISH);
        return outFormat.format(currentDateTime);
    }

    public static String getCurrentPhoneTime() {
        Calendar c = Calendar.getInstance();
        Date currentDateTime = c.getTime();
        SimpleDateFormat outFormat = new SimpleDateFormat(Constants.TIME_FORMAT, Locale.ENGLISH);
        return outFormat.format(currentDateTime);
    }

    public static void scheduleSync(Context context, String TAG){
        Log.d(TAG, "scheduleSync: ");
        Intent alarm = new Intent(context, MyService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, alarm, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

        Calendar scheduleTime = Calendar.getInstance();
        scheduleTime.setTimeInMillis(System.currentTimeMillis());

        if (scheduleTime.get(Calendar.HOUR_OF_DAY) > Constants.SCHEDULE_HOUR - 1) {
            scheduleTime.add(Calendar.DAY_OF_MONTH, 1);
        }
        scheduleTime.set(Calendar.HOUR_OF_DAY, Constants.SCHEDULE_HOUR);
        scheduleTime.set(Calendar.MINUTE, Constants.SCHEDULE_MINUTE);

        long timeWindow = TimeUnit.HOURS.toMillis(Constants.SCHEDULE_TIME_WINDOW_IN_HOURS);

        alarmManager.setWindow(AlarmManager.RTC_WAKEUP, scheduleTime.getTimeInMillis(), timeWindow, pendingIntent);

        String message = "Sync scheduled to: "
                + (new SimpleDateFormat(Constants.DATE_TIME_FORMAT, Locale.ENGLISH)).format(scheduleTime.getTime()) + "\n"
                + "time window: " + timeWindow / (1000 * 60) + " minutes";

        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "scheduleSync: " + message);
    }

    public static boolean isWiFiPresent(Context context) {
        boolean wifiConnected;

// deprecated
//        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
//        wifiCOnnected = mWifi.isConnected();

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                wifiConnected = true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                wifiConnected = false;
            } else {
                wifiConnected = false;
            }
        } else {
            // not connected to the internet
            wifiConnected = false;
        }

        return wifiConnected;
    }
}
