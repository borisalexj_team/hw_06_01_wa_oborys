package com.borisalexj.hw_06_01_wa_oborys;

/**
 * Created by user on 4/20/2017.
 */
public class Constants {
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final java.lang.String TIME_FORMAT = "HH:mm:ss";

    public static final String API_BASE_URL = "https://api.flickr.com/";
    public static final String API_METHOD_URL = "/services/feeds/photos_public.gne";

    public static final String SHARED_PREF_NAME = "hw_06_01_wa_oborys";
    public static final String SHARED_PREF_DATE = "date_update";
    public static final String SHARED_PREF_TIME = "time_update";
    public static final int SCHEDULE_HOUR = 2;
    public static final int SCHEDULE_MINUTE = 0;
    public static final int SCHEDULE_TIME_WINDOW_IN_HOURS = 2;
}
